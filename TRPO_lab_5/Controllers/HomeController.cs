﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TRPO_Lab3.Lib;

namespace TRPO_lab_5.Controllers
{
    public class HomeController : Controller
    {
       public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Calculator()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Calculator(string widthTop, string widthBot, string Height)
        {
            try
            {
                string result = (TropSquare.FindTropSquare(Convert.ToDouble(widthTop), Convert.ToDouble(widthBot), Convert.ToDouble(Height))).ToString();
                ViewData["result"] = result;
                return View("result");
            }
            catch(ArgumentException)
            {
                return View();
            }
        }
    }
}
