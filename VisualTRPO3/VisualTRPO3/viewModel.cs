﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using TRPO_Lab3.Lib;


namespace VisualTRPO3
{
    class viewModel : INotifyPropertyChanged
    {
        private double _widthTop;
        public double WidthTop { get
            { return _widthTop; }
            set {
                _widthTop = value;
                OnPropertyChanged("resultNumber"); }
        }
        private double _widthBot;
        public double WidthBot
        { get { return _widthBot; }
            set {
                _widthBot = value;
                OnPropertyChanged("resultNumber"); }
        }
        private double _heigth;
        public double Heigth
        {
            get { return _heigth; }
            set
            {
                _heigth = value;
                OnPropertyChanged("resultNumber");
            }
        }
        public string resultNumber { get => kekW(_widthTop, _widthBot, _heigth); }

        private string kekW(double _widthTop,double _widthBot,double _heigth)
        {
            try
            {
                return Convert.ToString(TropSquare.FindTropSquare(_widthTop, _widthBot, _heigth));
            }
            catch(ArgumentException)
            {
                return "Перепроверь";
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
